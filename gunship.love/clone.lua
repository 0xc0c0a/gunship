local function clone(prototype)
  -- Build a metatable for the clone instance, with keys/values copied from the
  -- metatable of the prototype.
  --
  -- It seems metatables don't have their metatable's __index respected when
  -- queried for operator metamethods, so a direct copy is the only way to
  -- do this.

  local metatable = {}
  for k, v in pairs(getmetatable(prototype) or {}) do
    -- XXX: Are there any metatable members that are tables that need cloning?
    -- Might need to support those later (if they exist).
    metatable[k] = v
  end

  -- Always make the instance metatable's __index point to the prototype.
  -- That's how inheritance works here!
  metatable.__index = prototype

  local instance = setmetatable({}, metatable)
  for k, v in pairs(prototype) do
    -- Table members need to be cloned, because tables are passed by reference.
    --
    -- We don't want to change the prototype's tables when we change them on
    -- the clone.
    if type(v) == 'table' then
      instance[k] = clone(v)
    end
  end

  return instance
end

return clone
