local setColor = love.graphics.setColor

function love.graphics.setColor(...)
  local args = {...}

  -- Table format.
  if #args == 1 then
    args = args[1]
  end

  for _, color in ipairs(args) do
    assert(color <= 1, 'You used a big color! This isn\'t 0.10!')
  end

  setColor(...)
end
