uniform vec3 foregroundColor = vec3(1.0);
uniform vec3 backgroundColor = vec3(0.0);

vec4 effect(vec4 color, sampler2D texture, vec2 textureCoords, vec2 screenCoords) {
  vec4 fragColor = texture2D(texture, textureCoords) * color;

  /* Average out the colors--grayscale it. */
  float average = (fragColor.r + fragColor.g + fragColor.b) / 3.0;

  mat4 thresholdMap = mat4( 0.0,  8.0,  2.0, 10.0,
                           12.0,  4.0, 14.0,  6.0,
                            3.0, 11.0,  1.0,  9.0,
                           15.0,  7.0, 13.0,  5.0 );
  ivec2 thresholdMapCoords = ivec2(mod(textureCoords*love_ScreenSize.xy, 4.0));

  if ((average * 16.0) > thresholdMap[thresholdMapCoords.x][thresholdMapCoords.y]) {
    fragColor.rgb = foregroundColor;
  } else {
    fragColor.rgb = backgroundColor;
  }

  return fragColor;
}
