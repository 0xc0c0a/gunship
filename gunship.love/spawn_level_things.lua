local t = {
  e = {
    Basic     = require 't.e.basic',
    Hardy     = require 't.e.hardy',
  }
}
local Things  = require 'things'
local Vector  = require 'vector'

return function(thingsData, bounds)
  local classTranslations = {
    ['o'] = t.e.Basic,
    ['O'] = t.e.Hardy,
  }

  local things = Things.new()

  for y, row in ipairs(thingsData) do
    for x, character in ipairs(row) do

      local class = classTranslations[character]
      if class ~= nil then

        local thing = class.new()
        thing.position = Vector.new(
          x/(#row+1)        * (bounds.max.x-bounds.min.x),
          y/(#thingsData+1) * (bounds.max.y-bounds.min.y)
        ):addVector(bounds.min)

        things:add(thing)

      end

    end
  end

  return things
end
