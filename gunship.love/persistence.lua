local persistence = {}

-- Shut up I know my way of saving stuff is retarded fight me.
-- It's a fun idea but holy shit I'm using CSV or something next time.

local function writeScoresFile(scores)
  local lines = { 'return {' }

  for levelId, levelTbl in pairs(scores) do
    table.insert(lines, string.format('  [%q] = {', levelId))

    for _, scoreTbl in ipairs(levelTbl) do
      table.insert(lines, string.format('    { name = %q, score = %d },', scoreTbl.name, scoreTbl.score))
    end

    table.insert(lines, '  },')
  end

  table.insert(lines, '}')

  love.filesystem.createDirectory('p')
  assert(love.filesystem.write('p/scores.lua', table.concat(lines, '\n')))
end

local function writeUnlockedFile(unlocked)
  local lines = { 'return {' }

  for levelId, _ in pairs(unlocked) do
    table.insert(lines, string.format('  [%q] = true,', levelId))
  end

  table.insert(lines, '}')

  love.filesystem.createDirectory('p')
  assert(love.filesystem.write('p/unlocked.lua', table.concat(lines, '\n')))
end

function persistence.getLevels()
  local levels = love.filesystem.load('p/levels.lua')()
  for k, v in pairs(levels) do
    -- The ID is handy in both places, but I don't want to double-up the data.
    v.id = k
  end
  return levels
end

function persistence.getLevelOrder()
  return love.filesystem.load('p/level_order.lua')()
end

function persistence.getScores()
  return love.filesystem.load('p/scores.lua')()
end

function persistence.addScore(levelId, name, score)
  local scores = persistence.getScores()
  scores[levelId] = scores[levelId] or {}

  table.insert(scores[levelId], { name = name, score = score })
  table.sort(scores[levelId], function(a, b) return a.score > b.score end)

  writeScoresFile(scores)
end

function persistence.getUnlocked()
  return love.filesystem.load('p/unlocked.lua')()
end

function persistence.unlock(levelId)
  local unlocked = persistence.getUnlocked()
  unlocked[levelId] = true

  writeUnlockedFile(unlocked)
end

return persistence
