local scenes = {
  stack = {}
}

function scenes.push(scene)
  table.insert(scenes.stack, scene)
end

function scenes.pop()
  return table.remove(scenes.stack)
end

function scenes.top()
  local stack = scenes.stack
  return stack[#stack]
end

function scenes.change(scene)
  scenes.pop()
  scenes.push(scene)
end

return scenes
