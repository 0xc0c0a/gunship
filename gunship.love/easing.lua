local easing = {}

function easing.lerp(a, b, t)
  return a + (b-a)*t
end

-- Now easings take (array-like) tables!

for name, fn in pairs(easing) do
  easing[name] = function(a, b, t)
    local aType = type(a)
    local bType = type(b)

    if aType == 'table' or bType == 'table' then
      assert(aType == 'table' and bType == 'table')
      assert(#a == #b)

      local easeTable = {}
      for i=1, #a do
        easeTable[i] = fn(a[i], b[i], t)
      end
      return easeTable
    end

    return fn(a, b, t)
  end
end

return easing
