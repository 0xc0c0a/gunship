local class                 = require 'class'
local defineTableCallbacks  = require 'define_table_callbacks'
local t = {}
local Vector                = require 'vector'

t.Thing = class{}

t.Thing.position      = Vector.new()
t.Thing.velocity      = Vector.new()
t.Thing.acceleration  = Vector.new()

t.Thing.radius = 0.5
t.Thing.dead = false

local number = 1

function t.Thing.construct(instance, id)
  instance.id = id

  instance.number = number
  number = number + 1
end

defineTableCallbacks(t.Thing, {'keypressed'}, function()
  return function() end
end)

function t.Thing:update(dt)
  self:updateVelocity(dt)
  self:updatePosition(dt)
end

function t.Thing:updateVelocity(dt)
  self.velocity = Vector.addVector(self.velocity, Vector.mulScalar(self.acceleration, dt))
end

function t.Thing:updatePosition(dt)
  self.position = Vector.addVector(self.position, Vector.mulScalar(self.velocity, dt))
end

return t.Thing
