local class   = require 'class'
local t = {
  b = {},
  Thing       = require 't.thing'
}
local Vector  = require 'vector'

t.b.Bullet = class(t.Thing)
t.b.Bullet.name = 'Bullet'
t.b.Bullet.cost = 1
t.b.Bullet.radius = 0.125
t.b.Bullet.angle = 0 -- Fly right.
t.b.Bullet.speed = 10

function t.b.Bullet.construct(instance, id)
  t.Thing.construct(instance, id)
end

function t.b.Bullet:update(dt)
  self.velocity = Vector.new(math.cos(self.angle), math.sin(self.angle)):mulScalar(self.speed)

  -- Bullets don't really use their acceleration, so just update position.
  self:updatePosition(dt)
end

return t.b.Bullet
