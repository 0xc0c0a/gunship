local class = require 'class'
local t = {
  b = {
    Bullet  = require 't.b.bullet'
  }
}

t.b.Dummy = class(th.b.Bullet)

function t.b.Dummy.construct(instance)
  t.b.Bullet.construct(instance, 'bullet_dummy')
end

return t.b.Dummy
