local class = require 'class'
local t = {
  b = {
    Bullet  = require 't.b.bullet'
  }
}

t.b.Basic = class(t.b.Bullet)
t.b.Basic.name = 'Slug'

function t.b.Basic.construct(instance)
  t.b.Bullet.construct(instance, 'bullet_basic')
end

return t.b.Basic
