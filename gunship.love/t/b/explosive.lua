local class = require 'class'
local t = {
  b = {
    Bullet  = require 't.b.bullet'
  }
}

t.b.Explosive = class(t.b.Bullet)
t.b.Explosive.name = 'Explosive'
t.b.Explosive.cost = 7
t.b.Explosive.explosionRadius = 2

function t.b.Explosive.construct(instance)
  t.b.Bullet.construct(instance, 'bullet_explosive')
end

return t.b.Explosive
