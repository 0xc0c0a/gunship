local class = require 'class'
local t = {
  Thing     = require 't.thing'
}

t.Explosion = class(t.Thing)

t.Explosion.radius = 1

t.Explosion.time = 0
t.Explosion.duration = 2
t.Explosion.firstFrame = true

function t.Explosion.construct(instance)
  t.Thing.construct(instance, 'explosion')
end

function t.Explosion:update(dt)
  self.time = self.time + dt
  if self.time >= self.duration then
    self.dead = true
  end
end

return t.Explosion
