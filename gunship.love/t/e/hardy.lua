local class = require 'class'
local t = {
  e = {
    Basic   = require 't.e.basic',
    Enemy   = require 't.e.enemy',
  }
}

t.e.Hardy = class(t.e.Basic)

t.e.Hardy.maxHealth = 2
t.e.Hardy.health = t.e.Hardy.maxHealth

function t.e.Hardy.construct(instance)
  t.e.Enemy.construct(instance, 'enemy_hardy')
end

function t.e.Hardy:hit()
  self.health = self.health - 1
  if self.health <= 0 then
    self.dead = true
  end
end

return t.e.Hardy
