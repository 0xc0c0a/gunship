local class = require 'class'
local t = {
  e = {
    Enemy   = require 't.e.enemy'
  }
}

t.e.Basic = class(t.e.Enemy)

function t.e.Basic.construct(instance)
  t.e.Enemy.construct(instance, 'enemy_basic')
end

return t.e.Basic
