local class = require 'class'
local t = {
  e = {},
  Thing     = require 't.thing'
}

t.e.Enemy = class(t.Thing)

t.e.Enemy.value = 1

function t.e.Enemy.construct(instance, id)
  t.Thing.construct(instance, id)
end

function t.e.Enemy:hit()
  self.dead = true
end

return t.e.Enemy
