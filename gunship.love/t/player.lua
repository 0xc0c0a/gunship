local class   = require 'class'
local clone   = require 'clone'
local t = {
  b = {
    Basic     = require 't.b.basic',
    Explosive = require 't.b.explosive'
  },
  Thing       = require 't.thing'
}
local Vector  = require 'vector'

t.Player = class(t.Thing)

-- Intentionally smaller than the visual representation.
-- Makes dodging feel better.
-- The enemies don't care if the player cheats. They don't have brains.
t.Player.radius = 0.25

t.Player.magazineIndex = 1

function t.Player.construct(instance, gameState)
  t.Thing.construct(instance, 'player')

  instance.gameState = gameState
end

function t.Player:update(dt)
  local maxSpeed = 5
  local accelerationMagnitude = 100
  local friction = 20

  self.acceleration.y = love.keyboard.isDown('up') and -1 or 0
  self.acceleration.y = self.acceleration.y + (love.keyboard.isDown('down') and 1 or 0)
  self.acceleration.y = self.acceleration.y * accelerationMagnitude

  -- Stop the player leaving the screen, by limiting their acceleration in the
  -- off-screen direction.

  if self.position.y < self.gameState.levelThingBounds.min.y+1.5 then
    self.acceleration.y = math.max(0, self.acceleration.y)
  end

  if self.position.y > self.gameState.levelThingBounds.max.y-1.5 then
    self.acceleration.y = math.min(0, self.acceleration.y)
  end

  self:updateVelocity(dt)

  -- Adjust speed.
  local speed = self.velocity:magnitude()
  if speed > 0 then
    speed = math.min(speed, maxSpeed)         -- Limit velocity to maxSpeed.
    speed = math.max(speed - friction*dt, 0)  -- Apply friction.

    self.velocity = self.velocity:normalize():mulScalar(speed)
  end

  self:updatePosition(dt)
end

function t.Player:keypressed(key)
  if key == 'space' then
    local class = self.gameState.magazine[self.magazineIndex]

    -- Fire bullet.
    local bullet = class.new()
    bullet.position = clone(self.position)
    self.gameState.things:add(bullet)

    -- Shift along magazine (for next shot).
    self.magazineIndex = (self.magazineIndex+1 <= #self.gameState.magazine) and self.magazineIndex+1 or 1

    -- Take away from the score by the bullet cost.
    self.gameState.score = self.gameState.score - bullet.cost
  end
end

return t.Player
