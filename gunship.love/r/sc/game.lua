local assets  = require 'assets'
local class   = require 'class'
local dip     = require 'dip'
local easing  = require 'easing'
local r = {
  sc = {
    Scene     = require 'r.sc.scene'
  }
}

--local unitScale = 64
local unitScale = 80

local function drawPlayer(player)
  love.graphics.push('all')
  love.graphics.setColor(0.625, 0.625, 0.625)

  love.graphics.circle('fill',
    player.position.x * unitScale,
    player.position.y * unitScale,
    2 * player.radius * unitScale,
    3) -- 3 makes the player a triangle.

  love.graphics.pop()
end

local function drawExplosion(explosion)
  local brightness = 1 - (explosion.time/explosion.duration)

  love.graphics.push('all')
  love.graphics.setColor(brightness, brightness, brightness)

  love.graphics.circle('fill',
    explosion.position.x * unitScale,
    explosion.position.y * unitScale,
    explosion.radius * unitScale)

  love.graphics.pop()
end

local function drawBasicEnemy(enemy)
  love.graphics.push('all')
  love.graphics.setColor(0.5, 0.5, 0.5)

  love.graphics.circle('fill',
    enemy.position.x * unitScale,
    enemy.position.y * unitScale,
    0.85 * enemy.radius * unitScale,
    4)

  love.graphics.pop()
end

local function drawHardyEnemy(enemy)
  local enemyBrightness = (enemy.health < enemy.maxHealth) and 0.0625 or 0.5

  love.graphics.push('all')
  love.graphics.setColor(
    enemyBrightness,
    enemyBrightness,
    enemyBrightness
  )

  love.graphics.circle('fill',
    enemy.position.x * unitScale,
    enemy.position.y * unitScale,
    0.85 * enemy.radius * unitScale,
    8)

  love.graphics.pop()
end

local function drawBulletBasic(bullet)
  love.graphics.push('all')
  love.graphics.setColor(1, 1, 1)

  love.graphics.circle('fill',
    bullet.position.x * unitScale,
    bullet.position.y * unitScale,
    bullet.radius * unitScale,
    8)

  love.graphics.pop()
end

local function drawBulletExplosive(bullet)
  love.graphics.push('all')
  love.graphics.setColor(1, 1, 1)

  love.graphics.rectangle('line',
    (bullet.position.x - bullet.radius) * unitScale,
    (bullet.position.y - bullet.radius) * unitScale,
    2*bullet.radius * unitScale,
    2*bullet.radius * unitScale)

  love.graphics.pop()
end

local function makeThingDraw(thingId)
  return ({
    ['player']            = drawPlayer,
    ['explosion']         = drawExplosion,
    ['enemy_basic']       = drawBasicEnemy,
    ['enemy_hardy']       = drawHardyEnemy,
    ['bullet_basic']      = drawBulletBasic,
    ['bullet_explosive']  = drawBulletExplosive,
  })[thingId]
end

r.sc.Game = class(r.sc.Scene)

function r.sc.Game:getForegroundColor()
  --[[
  local timeSinceExplosion = (love.timer.getTime()-self.state.lastExplosionTime)
  return unpack(easing.lerp({1, 1, 1}, {1, 1, 1}, math.min(2*timeSinceExplosion, 1)))
  ]]

  return 1, 1, 1
end

function r.sc.Game:getBackgroundColor()
  return 0, 0, 0
end

function r.sc.Game:drawPostProcessed()
  -- TODO: Render in order (player, enemies, bullets, explosions).
  for _, thing in ipairs(self.state.things.list) do
    makeThingDraw(thing.id)(thing)
  end

  self:drawHud()
end

function r.sc.Game:drawHud()
  local font       = assets.getBMFont('a/f/pcsenior24.fnt')
  local fontHeight = font:getHeight()

  local scoreText      = string.format('Score: %d', self.state.score)
  local scoreTextWidth = font:getWidth('Score: 99999')
  local scoreX         = love.graphics.getWidth()-24-scoreTextWidth,

  love.graphics.push('all')
  love.graphics.setColor(0.0625, 0.0625, 0.0625)

  love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), 1*unitScale)

  love.graphics.setColor(0, 0, 0)

  love.graphics.rectangle('fill',
    24,
    28,
    font:getWidth(self.state.levelName),
    fontHeight
  )
  love.graphics.rectangle('fill',
    scoreX,
    28,
    scoreTextWidth,
    fontHeight
  )

  love.graphics.setColor(1, 1, 1)
  love.graphics.setFont(font)

  love.graphics.print(self.state.levelName, 24, 28)
  love.graphics.print(scoreText, scoreX, 28)

  love.graphics.pop()
end

function r.sc.Game:drawDebug()
  love.graphics.push('all')

  -- Draw collision circles.

  love.graphics.setColor(0, 1, 0)

  for _, thing in ipairs(self.state.things.list) do
    love.graphics.circle(
      'line',
      thing.position.x * unitScale,
      thing.position.y * unitScale,
      thing.radius * unitScale)
  end

  -- Draw level thing boundary.

  love.graphics.setColor(1, 0, 0)

  love.graphics.rectangle(
    'line',
    self.state.levelThingBounds.min.x * unitScale,
    self.state.levelThingBounds.min.y * unitScale,
    (self.state.levelThingBounds.max.x - self.state.levelThingBounds.min.x) * unitScale,
    (self.state.levelThingBounds.max.y - self.state.levelThingBounds.min.y) * unitScale
  )

  -- Draw debug text.
  love.graphics.setColor(1, 0, 1)
  love.graphics.print(string.format('Score: %d', self.state.score))

  love.graphics.pop()
end

function r.sc.Game:draw()
  r.sc.Scene.draw(self)

  if dip.drawDebug then
    self:drawDebug()
  end
end

return r.sc.Game
