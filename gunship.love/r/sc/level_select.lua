local assets  = require 'assets'
local class   = require 'class'
local r = {
  sc = {
    Scene     = require 'r.sc.scene',
  },
  ui = {
    ListMenu  = require 'r.ui.list_menu',
    Marquee   = require 'r.ui.marquee',
  }
}

r.sc.LevelSelect = class(r.sc.Scene)

function r.sc.LevelSelect.construct(instance, state)
  r.sc.Scene.construct(instance, state)

  instance.infoMarquee = r.ui.Marquee.new({text = 'level select ... use [up]/[down] to move ... use [space](or [return]) to choose ... use [escape] to go back if you want to for some reason ... '})
    :setFont(assets.getBMFont('a/f/pcsenior18.fnt'))
end

function r.sc.LevelSelect:drawPostProcessed()
  -- Want to see something evil? >:)
  self.infoMarquee
    :setDirection('right')
    :setY(18)
    :draw()
    :setDirection('left')
    :setY(love.graphics.getHeight() - 2*self.infoMarquee:getFont():getHeight())
    :draw()

  local regularFont = assets.getBMFont('a/f/pcsenior18.fnt')

  r.ui.ListMenu.new(self.state.levelMenu)
    :setFont(regularFont)
    :centerPositionPercentage(0.5, 0.5)
    :draw()
end

return r.sc.LevelSelect
