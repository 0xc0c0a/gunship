local class = require 'class'
local r = {
  sc = {
    Scene   = require 'r.sc.scene'
  }
}

r.sc.Splash = class(r.sc.Scene)

function r.sc.Splash:getForegroundColor()
  return 1, 0, 0
end

function r.sc.Splash:getBackgroundColor()
  return 0, 0, 1
end

function r.sc.Splash:drawPostProcessed()
  local width, height = love.graphics.getDimensions()
  local splashSize = math.min(width, height)

  local image = self.state.image

  love.graphics.push()

  love.graphics.translate(width/2 - splashSize/2, height/2 - splashSize/2)
  love.graphics.scale(splashSize/image:getWidth(), splashSize/image:getHeight())

  love.graphics.draw(image)

  love.graphics.pop()
end

return r.sc.Splash
