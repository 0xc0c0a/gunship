local assets    = require 'assets'
local class     = require 'class'
local r = {
  sc = {
    Scene       = require 'r.sc.scene',
  },

  ui = {
    Background  = require 'r.ui.background',
    Column      = require 'r.ui.column',
    ListMenu    = require 'r.ui.list_menu',
    Marquee     = require 'r.ui.marquee',
    Spacer      = require 'r.ui.spacer',
    Text        = require 'r.ui.text',
  }
}

r.sc.MagazineSelect = class(r.sc.Scene)

function r.sc.MagazineSelect.construct(instance, state)
  r.sc.Scene.construct(instance, state)

  instance.infoMarquee = r.ui.Marquee.new({text = 'magazine select ... use [up]/[down] to move ... use [space](or [return]) to choose ... use [escape] to go back if you want to for some reason ... '})
    :setFont(assets.getBMFont('a/f/pcsenior18.fnt'))
end

function r.sc.MagazineSelect:drawPostProcessed()
  local regularFont = assets.getBMFont('a/f/pcsenior18.fnt')

  self.infoMarquee
    :setDirection('right')
    :setY(18)
    :draw()
    :setDirection('left')
    :setY(love.graphics.getHeight() - 2*self.infoMarquee:getFont():getHeight())
    :draw()

  r.ui.ListMenu.new(self.state.magazineMenu)
    :setFont(regularFont)
    :centerPositionPercentage(0.25, 0.5)
    :draw()

  local ammoColumn = r.ui.Column.new()
  ammoColumn:addChild(
    r.ui.Background.new():setColor(1, 1, 1):setPadding(10):setChild(
      r.ui.Text.new({text = 'Magazine:    '})
        :setFont(regularFont)
        :setColor(0, 0, 0)
    )
  )
  ammoColumn:addChild(r.ui.Spacer.new(0, regularFont:getHeight()))

  for i, ammoClass in ipairs(self.state.magazine) do
    ammoColumn:addChild(
      r.ui.Background.new():setColor(0, 0, 0):setPadding(10):setChild(
        r.ui.Text.new({text = string.format('%2d. %9s', i, ammoClass.name)})
          :setFont(regularFont)
          :setColor(1, 1, 1)
      )
    )
  end

  ammoColumn
    :centerXPercentage(0.75)
    :setY(0.2*love.graphics.getHeight())
    :draw()
end

return r.sc.MagazineSelect
