local assets  = require 'assets'
local class   = require 'class'
local r = {
  sc = {}
}

r.sc.Scene = class{}

function r.sc.Scene.construct(instance, state)
  instance.state = state

  --instance.postProcessingCanvas = love.graphics.newCanvas()
end

function r.sc.Scene:getForegroundColor()
  return 1, 1, 1
end

function r.sc.Scene:getBackgroundColor()
  return 0, 0, 0
end

function r.sc.Scene:draw()
  local postProcessingShader = assets.getShader('a/sh/screen.frag')
  postProcessingShader:send('foregroundColor', { self:getForegroundColor() })
  postProcessingShader:send('backgroundColor', { self:getBackgroundColor() })

  love.graphics.push('all')

  r.sc.Scene.postProcessingCanvas = r.sc.Scene.postProcessingCanvas or love.graphics.newCanvas()
  love.graphics.setCanvas(r.sc.Scene.postProcessingCanvas)

  love.graphics.push('all')
  love.graphics.setColor(0, 0, 0, 1)
  love.graphics.rectangle('fill', 0, 0, love.graphics.getDimensions())
  love.graphics.pop()

  self:drawPostProcessed()

  love.graphics.setCanvas()
  love.graphics.setShader(postProcessingShader)

  love.graphics.draw(r.sc.Scene.postProcessingCanvas)

  love.graphics.pop()
end

function r.sc.Scene:drawPostProcessed()
end

return r.sc.Scene
