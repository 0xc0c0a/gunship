local assets    = require 'assets'
local class     = require 'class'
local r = {
  sc = {
    Scene       = require 'r.sc.scene'
  },

  ui = {
    Background  = require 'r.ui.background',
    Column      = require 'r.ui.column',
    Marquee     = require 'r.ui.marquee',
    Spacer      = require 'r.ui.spacer',
    Text        = require 'r.ui.text',
  }
}

r.sc.Title = class(r.sc.Scene)

function r.sc.Title.construct(instance, state)
  r.sc.Scene.construct(instance, state)

  local marqueeFont    = assets.getBMFont('a/f/pcsenior18.fnt')
  local marqueeMessage = 'Made by Zak Stephens for the Linux Game Jam (2018) ... Thanks to codeman38 (for the PCSenior font) ... '

  instance.creditsMarquee = r.ui.Marquee.new({text = marqueeMessage})
    :setFont(marqueeFont)
    :setY(love.graphics.getHeight() - 2*marqueeFont:getHeight())
end

function r.sc.Title:getForegroundColor()
  return 1, 1, 1
end

function r.sc.Title:getBackgroundColor()
  return 0, 0, 0
end

function r.sc.Title:drawPostProcessed()
  local bigFont     = assets.getBMFont('a/f/pcsenior24.fnt')
  local regularFont = assets.getBMFont('a/f/pcsenior18.fnt')

  -- Draw the title.

  local column = r.ui.Column.new()

  column:addChild(
    r.ui.Text.new({text = 'Gunship:'})
      :setFont(bigFont)
      :setColor(0, 0, 0)
  )
  column:addChild(r.ui.Spacer.new(0, regularFont:getHeight()))
  column:addChild(
    r.ui.Text.new({text = 'Tactical Munitions Capitalism'})
      :setFont(regularFont)
      :setColor(0, 0, 0)
  )

  r.ui.Background.new()
    :setColor(1, 1, 1)
    :setChild(column)
    :setPadding(50)
    :centerPositionPercentage(0.5, 0.25)
    :draw()

  -- Draw the instructions.
  r.ui.Text.new({text = '[up]/[down] to move, [space] to shoot.\n\nFiring bullets costs points.\nDestroying enemies nets points.\nBreak even to unlock more levels.'})
    :setFont(regularFont)
    :centerPositionPercentage(0.5, 0.5)
    :draw()

  -- Draw the button prompt.

  local buttonPromptBrightness = 0.5 + 0.5*math.sin(2*math.pi*love.timer.getTime())

  r.ui.Text.new({text = 'PRESS SPACE'})
    :setFont(regularFont)
    :setColor(
      buttonPromptBrightness,
      buttonPromptBrightness,
      buttonPromptBrightness
    )
    :centerPositionPercentage(0.5, 0.75)
    :draw()

  -- Draw the version.

  local version = 'v2 (bug fixes)'

  r.ui.Text.new({ text = 'v2 (bug fixes)' })
    :setFont(regularFont)
    :setColor(0.25, 0.25, 0.25)
    :setX(love.graphics.getWidth() - regularFont:getWidth(version .. ' '))
    :setY(regularFont:getHeight())
    :draw()

  -- Draw the marquee with the credits and stuff.

  self.creditsMarquee:draw()
end

return r.sc.Title
