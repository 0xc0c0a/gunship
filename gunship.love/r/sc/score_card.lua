local assets  = require 'assets'
local class   = require 'class'
local r = {
  sc = {
    Scene     = require 'r.sc.scene'
  },

  ui = {
    Background  = require 'r.ui.background',
    Column      = require 'r.ui.column',
    Row         = require 'r.ui.row',
    Spacer      = require 'r.ui.spacer',
    Text        = require 'r.ui.text',
    TextInput   = require 'r.ui.text_input',
  }
}

r.sc.ScoreCard = class(r.sc.Scene)

function r.sc.ScoreCard:makePlayerScoreElement(font)
  local row = r.ui.Row.new()

  row:addChild(
    r.ui.Text.new({
      text = string.format('%2d. %06d - ',
        self.state.scoreIndex,
        self.state.scores[self.state.scoreIndex].score
      )
    })
    :setFont(font)
    :setColor(0, 0, 0)
  )

  row:addChild(
    r.ui.TextInput.new({
        text        = self.state.scoreNameInput.text,
        cursorIndex = self.state.scoreNameInput.cursorIndex,
      })
      :setFont(font)
      :setColor(0, 0, 0)
  )

  return row
end

function r.sc.ScoreCard:drawPostProcessed()
  local bigFont     = assets.getBMFont('a/f/pcsenior24.fnt')
  local regularFont = assets.getBMFont('a/f/pcsenior18.fnt')

  local column = r.ui.Column.new()
  column:addChild(
    r.ui.Text.new({text = self.state.levelName .. ' complete!'})
      :setFont(bigFont)
      :setColor(0, 0, 0)
  )

  if self.state.nextLevelUnlocked then
    local unlockedMessage = string.format('"%s" unlocked!', self.state.nextLevelName)

    local unlockedTextBrightness = 0.5 + 0.5*math.sin(2*math.pi*love.timer.getTime())
    local unlockedText = r.ui.Text.new({text = unlockedMessage})
      :setFont(regularFont)
      :setColor(
        unlockedTextBrightness,
        unlockedTextBrightness,
        unlockedTextBrightness
      )

    column:addChild(r.ui.Spacer.new(0, bigFont:getHeight()))
    column:addChild(unlockedText)
  end

  column:addChild(r.ui.Spacer.new(0, bigFont:getHeight()))
  column:addChild(r.ui.Text.new({text = 'High Scores:'}):setFont(regularFont):setColor(0, 0, 0))
  column:addChild(r.ui.Spacer.new(0, bigFont:getHeight()))

  -- High scores.
  -- These have already been sorted.
  for i=1, 10 do
    local v = self.state.scores[i]

    if i == self.state.scoreIndex then
      column:addChild(self:makePlayerScoreElement(regularFont))
    else
      local scoreText = (v ~= nil) and string.format('%2d. %06d - %s', i, v.score, v.name) or string.format('%2d.', i)

      column:addChild(
        r.ui.Text.new({text = scoreText})
          :setFont(regularFont)
          :setColor(0, 0, 0)
      )
    end
  end

  -- If the player sucks, their score gets tacked onto the bottom so they don't
  -- feel too bad.
  if self.state.scoreIndex > 10 then
    column:addChild(r.ui.Spacer.new(0, regularFont:getHeight()))
    column:addChild(r.ui.Text.new({text = '...'}):setFont(regularFont):setColor(0, 0, 0))
    column:addChild(r.ui.Spacer.new(0, regularFont:getHeight()))

    column:addChild(self:makePlayerScoreElement(regularFont))
  end

  local background = r.ui.Background.new()
    :setChild(column)
    --:setColor(0.75, 0.75, 0.75)
    :setColor(1, 1, 1)
    :setPadding(50)
    :centerPositionPercentage(0.5, 0.5)
    :draw()
end

return r.sc.ScoreCard
