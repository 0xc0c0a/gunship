local assets = require 'assets'
local class  = require 'class'
local r = {
  sc = {
    Scene    = require 'r.sc.scene',
  },

  ui = {
    Column   = require 'r.ui.column',
    ListMenu = require 'r.ui.list_menu',
    Spacer   = require 'r.ui.spacer',
    Text     = require 'r.ui.text',
  }
}

r.sc.Dip = class(r.sc.Scene)

function r.sc.Dip:getForegroundColor()
  return 1, 1, 1
end

function r.sc.Dip:getBackgroundColor()
  return 0, 0, 1
end

function r.sc.Dip:drawPostProcessed()
  local titleFont = assets.getBMFont('a/f/pcsenior24.fnt')
  local switchFont = assets.getBMFont('a/f/pcsenior18.fnt')

  r.ui.Column.new()
    :addChild(r.ui.ListMenu.new(self.state.switchMenu):setFont(switchFont))
    :centerPositionPercentage(0.5, 0.5)
    :draw()
end

return r.sc.Dip
