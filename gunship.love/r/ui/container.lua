local class = require 'class'
local r = {
  ui = {
    Element = require 'r.ui.element'
  }
}

r.ui.Container = class(r.ui.Element)

r.ui.Container.children = {}

function r.ui.Container:addChild(element)
  table.insert(self.children, element)
  return self
end

function r.ui.Container:sumChildValues(fn)
  local value = 0
  for _, element in ipairs(self.children) do
    value = value + fn(element)
  end
  return value
end

function r.ui.Container:greatestChildValue(fn)
  local value = 0
  for _, element in ipairs(self.children) do
    value = math.max(value, fn(element))
  end
  return value
end

return r.ui.Container
