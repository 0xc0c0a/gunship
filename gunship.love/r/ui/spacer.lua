local class = require 'class'
local r = {
  ui = {
    Element = require 'r.ui.element'
  }
}

r.ui.Spacer = class(r.ui.Element)

function r.ui.Spacer.construct(instance, width, height)
  r.ui.Element.construct(instance, {})

  instance.width  = width
  instance.height = height
end

return r.ui.Spacer
