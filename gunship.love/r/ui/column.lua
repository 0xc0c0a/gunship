local class   = require 'class'
local r = {
  ui = {
    Container = require 'r.ui.container'
  }
}

r.ui.Column = class(r.ui.Container)

function r.ui.Column:getWidth()
  return self:greatestChildValue(function(element) return element:getWidth() end)
end

function r.ui.Column:getHeight()
  return self:sumChildValues(function(element) return element:getHeight() end)
end

function r.ui.Column:draw()
  love.graphics.push()
  love.graphics.translate(self:getPosition())

  for _, element in ipairs(self.children) do
    element:draw()
    love.graphics.translate(0, math.floor(element:getY() + element:getHeight()))
  end

  love.graphics.pop()
end

return r.ui.Column
