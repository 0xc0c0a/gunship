local class = require 'class'
local r = {
  ui = {
    Text    = require 'r.ui.text',
  }
}

-- Text marquee class.
-- To be rewritten in terms of mixins later.

r.ui.Marquee = class(r.ui.Text)
r.ui.Marquee.direction = 'left'

-- Domain-specific functions.

function r.ui.Marquee:setDirection(direction)
  self.direction = direction
  return self
end

-- Overrides that make it act like a screen-width marquee.

function r.ui.Marquee:getX()
  return 0
end

function r.ui.Marquee:getWidth()
  return love.graphics.getWidth()
end

function r.ui.Marquee:draw()
  local text      = self.state.text

  local font      = self:getFont()
  local textWidth = font:getWidth(text)

  love.graphics.push('all')
  love.graphics.translate(-textWidth, 0)
  love.graphics.translate(
    (self.direction == 'left' and -1 or 1) * math.floor(love.timer.getTime() * font:getWidth('a') * 5 * 180/60) % textWidth,
    0
  )

  love.graphics.setFont(font)

  for i=1, math.floor(self:getWidth() / textWidth) + 2 do

    love.graphics.print(text,
      self:getX() + (i-1)*textWidth,
      self:getY())

  end

  love.graphics.pop()

  return self
end

return r.ui.Marquee
