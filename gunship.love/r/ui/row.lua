local class   = require 'class'
local r = {
  ui = {
    Container = require 'r.ui.container'
  }
}

r.ui.Row = class(r.ui.Container)

function r.ui.Row:getWidth()
  return self:sumChildValues(function(element) return element:getWidth() end)
end

function r.ui.Row:getHeight()
  return self:greatestChildValue(function(element) return element:getHeight() end)
end

function r.ui.Row:draw()
  love.graphics.push()
  love.graphics.translate(self:getPosition())

  for _, element in ipairs(self.children) do
    element:draw()
    love.graphics.translate(element:getX() + element:getWidth(), 0)
  end

  love.graphics.pop()
end

return r.ui.Row
