local class = require 'class'
local r = {
  ui = {
    Element = require 'r.ui.element'
  }
}

r.ui.Background = class(r.ui.Element)
r.ui.Background.child = nil

function r.ui.Background:getPadding()
  return self.padding or 0
end

function r.ui.Background:setPadding(padding)
  self.padding = padding
  return self -- Method chaining.
end

function r.ui.Background:getWidth()
  if self.width then
    return self.width
  end

  if self.child == nil then
    return 0
  end

  return self.child:getWidth() + 2*self:getPadding()
end

function r.ui.Background:getHeight()
  if self.height then
    return self.height
  end

  if self.child == nil then
    return 0
  end

  return self.child:getHeight() + 2*self:getPadding()
end

function r.ui.Background:draw()
  if self.child == nil then
    return
  end

  love.graphics.push()
  love.graphics.translate(self:getPosition())

  love.graphics.push('all')
  love.graphics.setColor(self:getColor())
  love.graphics.rectangle('fill', 0, 0, self:getDimensions())
  love.graphics.pop()

  love.graphics.translate(self:getPadding(), self:getPadding())

  self.child:draw()

  love.graphics.pop()
end

function r.ui.Background:setChild(element)
  self.child = element
  return self -- Method chaining.
end

return r.ui.Background
