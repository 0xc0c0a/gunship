local class = require 'class'
local r = {
  ui = {
    Text    = require 'r.ui.text'
  }
}
local utf8  = require 'utf8'

r.ui.TextInput = class(r.ui.Text)

function r.ui.TextInput:draw()
  r.ui.Text.draw(self)

  local font = self:getFont()

  local cursorOffset = utf8.offset(self.state.text, self.state.cursorIndex)

  local cursorX      = self.state.cursorIndex == 0 and 0 or font:getWidth(self.state.text:sub(1, cursorOffset))
  local cursorY      = 0
  local cursorWidth  = font:getWidth(self.state.cursorIndex == 0 and ' ' or self.state.text:sub(cursorOffset, cursorOffset))
  local cursorHeight = font:getHeight()

  love.graphics.push('all')
  love.graphics.translate(self:getPosition())
  love.graphics.setColor(self:getColor())
  love.graphics.rectangle('line', cursorX, cursorY, cursorWidth, cursorHeight)
  love.graphics.pop()
end

return r.ui.TextInput
