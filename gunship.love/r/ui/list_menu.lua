local assets  = require 'assets'
local class   = require 'class'
local r = {
  ui = {
    Background  = require 'r.ui.background',
    Column      = require 'r.ui.column',
    Element     = require 'r.ui.element',
    Text        = require 'r.ui.text',
  }
}

r.ui.ListMenu = class(r.ui.Element)

function r.ui.ListMenu:getFont()
  return self.font or love.graphics.getFont()
end

function r.ui.ListMenu:setFont(font)
  self.font = font
  return self
end

-- This is a shitty hack but it's 4:30 in the morning and I don't care.
function r.ui.ListMenu:centerPositionPercentage(xPercentage, yPercentage)
  self.xPercentage = xPercentage
  self.yPercentage = yPercentage

  return self
end

function r.ui.ListMenu:getPositionPercentage()
  return (self.xPercentage or 0), (self.yPercentage or 0)
end

function r.ui.ListMenu:draw()
  local column = r.ui.Column.new()

  for i, item in ipairs(self.state.items) do
    local text = r.ui.Text.new({text = item.text})
      :setFont(self:getFont())
      :setColor(1, 1, 1)

    local background = r.ui.Background.new()
      :setColor(0, 0, 0)
      :setPadding(15)
      :setChild(text)

    if i == self.state.selectionIndex then
      background:setColor(1, 1, 1)
      text:setColor(0, 0, 0)
    elseif item.disabled then
      background:setColor(0.125, 0.125, 0.125)
        :setChild(r.ui.Background.new():setColor(0, 0, 0):setChild(text))
    end

    column:addChild(background)
  end

  local columnWidth = column:getWidth()
  for _, element in ipairs(column.children) do
    element.width = columnWidth
  end

  -- XXX: Fix this nasty hack that breaks my shit.
  column:centerPositionPercentage(self:getPositionPercentage()):draw()
end

return r.ui.ListMenu
