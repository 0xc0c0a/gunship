local class = require 'class'
local r = {
  ui = {}
}

r.ui.Element = class{}

function r.ui.Element.construct(instance, state)
  instance.state = state
end

function r.ui.Element:getX()
  return self.x or 0
end

function r.ui.Element:setX(x)
  self.x = x
  return self
end

function r.ui.Element:getY()
  return self.y or 0
end

function r.ui.Element:setY(y)
  self.y = y
  return self
end

function r.ui.Element:getPosition()
  return self:getX(), self:getY()
end

function r.ui.Element:centerXPercentage(x)
  return self:setX(math.floor(x*love.graphics.getWidth()  - 0.5*self:getWidth()))
end

function r.ui.Element:centerYPercentage(y)
  return self:setY(math.floor(y*love.graphics.getHeight() - 0.5*self:getHeight()))
end

-- This is just plain handy.
function r.ui.Element:centerPositionPercentage(x, y)
  return self:centerXPercentage(x):centerYPercentage(y)
end

function r.ui.Element:getWidth()
  return self.width or 0
end

function r.ui.Element:getHeight()
  return self.height or 0
end

function r.ui.Element:getDimensions()
  return self:getWidth(), self:getHeight()
end

function r.ui.Element:getColor()
  if self.color then
    return self.color.r, self.color.g, self.color.b
  end
  return 1, 1, 1
end

function r.ui.Element:setColor(r, g, b)
  self.color = { r = r, g = g, b = b }
  return self -- Method chaining.
end

function r.ui.Element:draw()
end

return r.ui.Element
