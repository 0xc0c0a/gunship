local class = require 'class'
local r = {
  ui = {
    Element = require 'r.ui.element'
  }
}

r.ui.Text = class(r.ui.Element)

function r.ui.Text:getHeight()
  return self:getFont():getHeight()
end

function r.ui.Text:getWidth()
  return self:getFont():getWidth(self.state.text)
end

function r.ui.Text:draw()
  love.graphics.push('all')

  love.graphics.setColor(self:getColor())
  love.graphics.setFont(self:getFont())

  love.graphics.print(self.state.text, math.floor(self:getX()), math.floor(self:getY()))

  love.graphics.pop()
end

function r.ui.Text:getFont()
  return self.font or love.graphics.getFont()
end

function r.ui.Text:setFont(font)
  self.font = font
  return self -- Method chaining
end

return r.ui.Text
