function love.conf(t)
  t.window.title = 'Gunship: Tactical Munitions Capitalism'

  t.window.width = 1280
  t.window.height = 720
  
  t.version = '11.0'
end
