return function(tbl, callbacks, closure)
  for _, callback in ipairs(callbacks) do
    tbl[callback] = closure(callback)
  end
end
