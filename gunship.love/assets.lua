local assets = {
  bmFonts = {},
  images = {},
  shaders = {}
}

function assets.loadBMFont(path)
  assets.bmFonts[path] = love.graphics.newFont(path)
end

function assets.loadImage(path, ...)
  assets.images[path] = love.graphics.newImage(path, ...)
end

function assets.loadShader(path)
  assets.shaders[path] = love.graphics.newShader(path)
end

function assets.getBMFont(path)
  return assets.bmFonts[path]
end

function assets.getImage(path)
  return assets.images[path]
end

function assets.getShader(path)
  return assets.shaders[path]
end

return assets
