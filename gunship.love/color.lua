local color = {}

function color.withAlpha(alpha)
  return setmetatable({}, {
    __index = function(_, key)
      return function(...)
        local r, g, b = color[key](...)
        return r, g, b, alpha
      end
    end
  })
end

function color.grayscale(intensity)
  return intensity, intensity, intensity
end

local function defineColor(r, g, b, name)
  color[name] = function()
    return r, g, b
  end
end

defineColor(1, 1, 1, 'white')
defineColor(0, 0, 0, 'black')

-- I'll probably just use these for debugging

defineColor(1, 0, 0, 'red')
defineColor(0, 1, 0, 'green')
defineColor(0, 0, 1, 'blue')
defineColor(1, 1, 0, 'yellow')
defineColor(1, 0, 1, 'magenta')
defineColor(0, 1, 1, 'cyan')

return color
