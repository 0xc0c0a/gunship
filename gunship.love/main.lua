local assets                = require 'assets'
local defineTableCallbacks  = require 'define_table_callbacks'
local loveCallbacks         = require 'love_callbacks'
local sc = {
  Dip                       = require 'sc.dip',
  Title                     = require 'sc.title',
}
local scenes                = require 'scenes'

require 'complain_about_big_colors'

defineTableCallbacks(love, loveCallbacks, function(callback)
  return function(...)
    local scene = scenes.top()
    scene[callback](scene, ...)
  end
end)

function love.load()
  -- Load everything (the game is tiny, it's fine).
  assets.loadBMFont('a/f/pcsenior18.fnt')
  assets.loadBMFont('a/f/pcsenior24.fnt')

  assets.loadImage('a/i/proprietary.png')
  assets.loadShader('a/sh/screen.frag')

  scenes.push(sc.Title.new())
end

function love.keypressed(key, ...)
  local scene = scenes.top()

  if key == '`' then
    if scene:getName() == 'dip' then
      scenes.pop()
    else
      scenes.push(sc.Dip.new())
    end

    return
  end

  scene:keypressed(key, ...)
end
