local class = require 'class'
local ui = {}
local utf8  = require 'utf8'

ui.TextInput = class{}

ui.TextInput.text = ''
ui.TextInput.cursorIndex = 0
ui.TextInput.lengthLimit = 24

function ui.TextInput:moveLeft()
  self.cursorIndex = math.max(self.cursorIndex-1, 0)
end

function ui.TextInput:moveRight()
  self.cursorIndex = math.min(self.cursorIndex+1, utf8.len(self.text))
end

function ui.TextInput:insert(textInput)
  if utf8.len(self.text)+1 > self.lengthLimit then
    return
  end

  local textHead = self.text:sub(1, self.cursorIndex == 0 and 0 or utf8.offset(self.text, self.cursorIndex))
  local textTail = self.text:sub(utf8.offset(self.text, self.cursorIndex+1), #self.text)

  self.text = textHead .. textInput .. textTail
  self.cursorIndex = self.cursorIndex + utf8.len(textInput)
end

function ui.TextInput:backspace()
  if utf8.len(self.text) == 0 or self.cursorIndex == 0 then
    return
  end

  local textHead = ''
  if self.cursorIndex > 1 then
    textHead = self.text:sub(1, utf8.offset(self.text, self.cursorIndex-1))
  end
  local textTail = self.text:sub(utf8.offset(self.text, self.cursorIndex+1), #self.text)

  self.text = textHead .. textTail
  self.cursorIndex = self.cursorIndex - 1
end

return ui.TextInput
