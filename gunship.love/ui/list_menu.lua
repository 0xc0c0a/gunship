local class = require 'class'
local ui = {}

ui.ListMenu = class{}

ui.ListMenu.items = {}
ui.ListMenu.selectionIndex = nil

function ui.ListMenu:moveUp()
  local selectionIndex = self.selectionIndex or #self.items+1

  for i=selectionIndex-1, selectionIndex-#self.items, -1 do
    if i < 1 then
      i = i + #self.items
    end

    if not self.items[i].disabled then
      self.selectionIndex = i
      return
    end
  end
end

function ui.ListMenu:moveDown()
  local selectionIndex = self.selectionIndex or 0

  for i=selectionIndex+1, selectionIndex+#self.items do
    if i > #self.items then
      i = i - #self.items
    end

    if not self.items[i].disabled then
      self.selectionIndex = i
      return
    end
  end
end

function ui.ListMenu:select()
  local selectionIndex = self.selectionIndex
  if selectionIndex then
    self.items[selectionIndex].selectAction()
  end
end

function ui.ListMenu:addItem(item)
  table.insert(self.items, item)

  if self.selectionIndex == nil and not item.disabled then
    self.selectionIndex = #self.items
  end

  return item
end

return ui.ListMenu
