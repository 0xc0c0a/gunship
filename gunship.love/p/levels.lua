return {

  ['level1'] = {
    name = 'Gentle Introduction',
    unlocks = 'level2',
    things = {
      {' ', ' ', ' ', ' ', ' '},
      {' ', 'o', 'o', 'o', ' '},
      {' ', 'o', 'o', 'o', ' '},
      {' ', 'o', 'o', 'o', ' '},
      {' ', ' ', ' ', ' ', ' '},
    },
  },

  ['level2'] = {
    name = 'New Stuff',
    unlocks = 'playground',
    things = {
      {'O', 'O', 'O', ' ', ' '},
      {'o', 'O', 'O', ' ', ' '},
      {'O', 'O', 'O', 'O', 'O'},
      {' ', ' ', 'o', 'O', 'O'},
      {' ', ' ', 'O', 'O', 'O'},
    },
  },

  ['playground'] = {
    name = 'The Playground',
    things = {
      {'O', 'o', 'o', 'o', 'o'},
        {'O', ' ', 'o', 'o'},
      {'O', 'o', 'o', 'O', 'o'},
      {' ', 'o', 'o', ' ', 'o'},
      {'O', ' ', ' ', ' ', 'o'},
    },
  }
}
