local Vector = require 'vector'

local collision = {}

function collision.circleCircle(a, b)
  return Vector.subVector(a.position, b.position):magnitude() <= (a.radius+b.radius)
end

return collision
