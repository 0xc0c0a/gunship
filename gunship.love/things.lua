local class = require 'class'

local Things = class{}

Things.list = {}

function Things:add(thing)
  table.insert(self.list, thing)
end

function Things:removeDead()
  -- Build a new list of things that aren't dead, and use that as the new
  -- list.

  local notDead = {}

  for _, thing in ipairs(self.list) do
    if not thing.dead then
      table.insert(notDead, thing)
    end
  end

  self.list = notDead
end

return Things
