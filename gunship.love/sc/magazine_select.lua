local class         = require 'class'
local r = {
  sc = {
    MagazineSelect  = require 'r.sc.magazine_select',
  }
}
local sc = {
  Game              = require 'sc.game',
  Scene             = require 'sc.scene',
}
local scenes        = require 'scenes'
local t = {
  b = {
    Basic           = require 't.b.basic',
    Explosive       = require 't.b.explosive',
  }
}
local ui = {
  ListMenu          = require 'ui.list_menu',
}

sc.MagazineSelect = class(sc.Scene)

function sc.MagazineSelect.construct(instance, level)
  local ammoClasses = {
    t.b.Basic,
    t.b.Explosive,
  }

  local state = {}
  state.magazine = {}
  state.magazineMenu = ui.ListMenu.new()

  local playMenuItem = {
    text = "Let's Rock!",
    disabled = true,

    selectAction = function()
      assert(#instance.state.magazine > 0)
      scenes.change(sc.Game.new(level, state.magazine))
    end,
  }

  for i, ammoClass in ipairs(ammoClasses) do
    state.magazineMenu:addItem{
      text = string.format('Add %s', ammoClass.name:lower()),

      selectAction = function()
        if #state.magazine < 10 then
          table.insert(state.magazine, ammoClass)
          playMenuItem.disabled = false
        end
      end,
    }
  end

  state.magazineMenu:addItem{
    text = 'Reset',

    selectAction = function()
      state.magazine = {}
      playMenuItem.disabled = true
    end,
  }

  state.magazineMenu:addItem(playMenuItem)

  sc.Scene.construct(instance, state, r.sc.MagazineSelect.new(state))
end

function sc.MagazineSelect:keypressed(key)
  if key == 'down' then
    self.state.magazineMenu:moveDown()
  end

  if key == 'up' then
    self.state.magazineMenu:moveUp()
  end

  if key == 'space' or key == 'return' then
    self.state.magazineMenu:select()
  end

  if key == 'escape' or key == 'backspace' then
    -- XXX: Just dodging a circular dependency.
    scenes.change(require('sc.level_select').new())
  end
end

return sc.MagazineSelect
