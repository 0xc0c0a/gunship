local class   = require 'class'
local r = {
  sc = {
    Title     = require 'r.sc.title'
  }
}
local sc = {
  Scene       = require 'sc.scene',
  LevelSelect = require 'sc.level_select',
}
local scenes  = require 'scenes'

sc.Title = class(sc.Scene)

function sc.Title.construct(instance)
  local state = {}

  sc.Scene.construct(instance, state, r.sc.Title.new(state))
end

function sc.Title:keypressed(key)
  if key == 'space' or key == 'return' then
    scenes.push(sc.LevelSelect.new())
  end
end

return sc.Title
