local class       = require 'class'
local persistence = require 'persistence'
local r = {
  sc = {
    LevelSelect   = require 'r.sc.level_select'
  }
}
local sc = {
  MagazineSelect  = require 'sc.magazine_select',
  Scene           = require 'sc.scene',
}
local scenes      = require 'scenes'
local ui = {
  ListMenu        = require 'ui.list_menu'
}

sc.LevelSelect = class(sc.Scene)

function sc.LevelSelect.construct(instance)
  local levels     = persistence.getLevels()
  local levelOrder = persistence.getLevelOrder()
  local unlocked   = persistence.getUnlocked()

  local state = {}
  state.levelMenu = ui.ListMenu.new()

  for i, levelId in ipairs(levelOrder) do
    local level = levels[levelId]

    state.levelMenu:addItem{
      text     = unlocked[levelId] and level.name or '** LOCKED **',
      disabled = not unlocked[levelId],

      selectAction = function()
        scenes.change(sc.MagazineSelect.new(level))
      end
    }
  end

  sc.Scene.construct(instance, state, r.sc.LevelSelect.new(state))
end

function sc.LevelSelect:keypressed(key)
  if key == 'down' then
    self.state.levelMenu:moveDown()
  end

  if key == 'up' then
    self.state.levelMenu:moveUp()
  end

  if key == 'return' or key == 'space' then
    self.state.levelMenu:select()
  end

  if key == 'escape' or key == 'backspace' then
    scenes.pop()
  end
end

return sc.LevelSelect
