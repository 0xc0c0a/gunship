local class = require 'class'
local dip   = require 'dip'
local r = {
  sc = {
    Dip     = require 'r.sc.dip',
  }
}
local sc = {
  Scene     = require 'sc.scene',
}
local ui = {
  ListMenu  = require 'ui.list_menu',
}

sc.Dip = class(sc.Scene)

function sc.Dip.construct(instance)
  local state = {
    switchMenu = ui.ListMenu.new()
  }

  for k, v in pairs(dip) do
    state.switchMenu:addItem{
      text = k,
      selectAction = function()
        dip[k] = not dip[k]
      end
    }
  end

  sc.Scene.construct(instance, state, r.sc.Dip.new(state))
end

function sc.Dip:getName()
  return 'dip'
end

function sc.Dip:keypressed(key)
  if key == 'down' then
    self.state.switchMenu:moveDown()
  end

  if key == 'up' then
    self.state.switchMenu:moveUp()
  end

  if key == 'return' or key == 'space' then
    self.state.switchMenu:select()
  end
end

return sc.Dip
