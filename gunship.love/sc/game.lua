local class                 = require 'class'
local clone                 = require 'clone'
local collision             = require 'collision'
local defineTableCallbacks  = require 'define_table_callbacks'
local e = {
  string                    = require 'e.string'
}
local r = {
  sc = {
    Game                    = require 'r.sc.game'
  }
}
local sc = {
  Scene                     = require 'sc.scene',
  ScoreCard                 = require 'sc.score_card',
}
local scenes                = require 'scenes'
local spawnLevelThings      = require 'spawn_level_things'
local t = {
  e = {
    Basic                   = require 't.e.basic',
    Enemy                   = require 't.e.enemy',
    Hardy                   = require 't.e.hardy',
  },
  Explosion                 = require 't.explosion',
  Player                    = require 't.player',
}
local Things                = require 'things'
local Vector                = require 'vector'

sc.Game = class(sc.Scene)

function sc.Game.construct(instance, level, magazine)
  local state = {}
  state.drawDebug = false
  state.drawGraphics = true
  state.lastExplosionTime = 0
  state.levelId = level.id
  state.levelName = level.name
  state.levelThingBounds  = { min = Vector.new(5, 1), max = Vector.new(16, 9) }
  state.magazine = magazine
  state.score = 0
  state.things = spawnLevelThings(level.things, state.levelThingBounds)

  local player = t.Player.new(state)
  player.position = Vector.new(1, 1+4)
  state.things:add(player)

  sc.Scene.construct(instance, state, r.sc.Game.new(state))
end

defineTableCallbacks(sc.Game, {'update'}, function(callback)
  return function(self, ...)
    for _, thing in ipairs(self.state.things.list) do
      thing[callback](thing, ...)
    end
  end
end)

function sc.Game:keypressed(key, scancode, isRepeat)
  for _, thing in ipairs(self.state.things.list) do
    thing:keypressed(key, scancode, isRepeat)
  end
end

function sc.Game:update(dt)
  self:killOffScreenBullets()
  self.state.things:removeDead()

  for _, thing in ipairs(self.state.things.list) do
    thing:update(dt)
  end

  -- Test for collisions between all pairs of distinct things.
  self.doNotCollide = self.doNotCollide or {}

  for _, tA in ipairs(self.state.things.list) do
    for _, tB in ipairs(self.state.things.list) do

      -- Things can't collide with themselves.
      --
      -- Things collide with things with a higher number (this enforces one-way
      -- collisions).

      if tA.number ~= tB.number and tA.number > tB.number and collision.circleCircle(tA, tB) then
        self.doNotCollide[tA.number] = self.doNotCollide[tA.number] or {}

        -- Things can only collide once.

        if not tA.id == 'explosion' or not self.doNotCollide[tA.number][tB.number] then
          self:resolveCollision(tA, tB)
          self.doNotCollide[tA.number][tB.number] = true
        end
      end

    end
  end

  self:endGameIfOnlyPlayerIsLeft()
end

function sc.Game:resolveCollision(tA, tB)
  if e.string.starts(tB.id, 'enemy') and not tB.dead then

    -- XXX: Refactor this confusing nonsense.
    -- The behaviour isn't complicated, just the implementation.

    -- When an enemy gets hit, spawn an explosion.
    -- If it's an explosive bullet, make it really big.
    local explosion = t.Explosion.new()
    explosion.position = clone(tB.position)
    explosion.radius   = tA.id == 'bullet_explosive' and tA.explosionRadius or tB.radius

    if e.string.starts(tA.id, 'bullet') then
      tA.dead = true
    end

    if tA.id == 'explosion' then

      -- XXX: Refactor; don't depend on the radius.
      if tA.radius == tB.radius then
        -- "Enemy got hit with a normal bullet" (effect) explosion.
        tB:hit()
      else
        -- (Assuming big explosion, because size difference.)
        tB.dead = true
      end

      -- Add the enemy's value to our score, if we killed them.
      if tB.dead then
        self.state.score = self.state.score + tB.value
      end

      -- Set the last explosion time to now.
      -- (The renderer uses this for fancy effects.)
      self.state.lastExplosionTime = love.timer.getTime()

      -- The explosion was created because the enemy got hit (by an explosion).
      --
      -- This is an effect.
      -- It may hit other enemies (but won't, but I might make it do that with
      -- a larger radius later), but it's not allowed to the hit the enemy that
      -- it was created from.

      -- Don't hit the enemy with the new explosion this explosion created.

      self.doNotCollide[explosion.number] = self.doNotCollide[explosion.number] or {}
      self.doNotCollide[explosion.number][tB.number] = true
    end

    self.state.things:add(explosion)
  end
end

function sc.Game:killOffScreenBullets()
  for _, thing in ipairs(self.state.things.list) do

    if e.string.starts(thing.id, 'bullet') then
       if thing.position.x+thing.radius < 0 or
          thing.position.x-thing.radius > 16 or
          thing.position.y+thing.radius < 0 or
          thing.position.y-thing.radius > 9 then

         thing.dead = true

       end
    end

  end
end

function sc.Game:endGameIfOnlyPlayerIsLeft()
  local thingsList = self.state.things.list

  if (#thingsList == 1) and (thingsList[1].id == 'player') then
    -- TODO: Pass some level data.
    scenes.change(sc.ScoreCard.new(self.state.levelId, self.state.score))
  end
end

return sc.Game
