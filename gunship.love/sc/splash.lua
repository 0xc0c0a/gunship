local class = require 'class'
local r = {
  sc = {
    Splash  = require 'r.sc.splash'
  }
}
local sc = {
  Scene     = require 'sc.scene'
}

sc.Splash = class(sc.Scene)

function sc.Splash.construct(instance, image)
  local state = {
    image = image
  }

  sc.Scene.construct(instance, state, r.sc.Splash.new(state))
end

return sc.Splash
