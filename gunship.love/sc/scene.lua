local class                 = require 'class'
local defineTableCallbacks  = require 'define_table_callbacks'
local loveCallbacks         = require 'love_callbacks'
local sc = {}

sc.Scene = class{}

function sc.Scene.construct(instance, state, renderer)
  instance.state = state
  instance.renderer = renderer
end

-- I don't feel like building canonical names into my class library, so I'll
-- use this when I really need to work out what the state I'm looking at is.
--
-- (Totally going to be lazy and not define it for most of them, though.)
--
function sc.Scene:getName()
  return nil
end

-- Love callbacks

defineTableCallbacks(sc.Scene, loveCallbacks, function()
  return function() end
end)

function sc.Scene:draw()
  self.renderer:draw()
end

return sc.Scene
