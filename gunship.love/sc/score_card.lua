local class       = require 'class'
local persistence = require 'persistence'
local r = {
  sc = {
    ScoreCard     = require 'r.sc.score_card'
  }
}
local sc = {
  Scene           = require 'sc.scene',
}
local scenes      = require 'scenes'
local ui = {
  TextInput       = require 'ui.text_input'
}
local utf8        = require 'utf8'

sc.ScoreCard = class(sc.Scene)

function sc.ScoreCard.construct(instance, levelId, score)
  local levels   = persistence.getLevels()
  local unlocked = persistence.getUnlocked()

  -- Insert new score into the scores array for the current level, sorted.
  -- Cache the index of the score, it's used in the renderer.
  local scores   = persistence.getScores()[levelId] or {}

  local scoreIndex = 1
  while scoreIndex < #scores+1 do
    if scores[scoreIndex] and score > scores[scoreIndex].score then
      break
    end
    scoreIndex = scoreIndex + 1
  end

  table.insert(scores, scoreIndex, { score = score })

  local level = levels[levelId]

  local state = {}
  state.scores         = scores
  state.scoreIndex     = scoreIndex
  state.scoreNameInput = ui.TextInput.new()
  state.levelId        = level.id
  state.levelName      = level.name

  -- Unlock the next level, if it isn't unlocked already and we broke even.
  if level.unlocks ~= nil and not unlocked[level.unlocks] and score >= 0 then
    persistence.unlock(level.unlocks)

    state.nextLevelUnlocked = true
    state.nextLevelName = levels[level.unlocks].name
  end

  sc.Scene.construct(instance, state, r.sc.ScoreCard.new(state))
end

function sc.ScoreCard:textinput(text)
  self.state.scoreNameInput:insert(text)
end

function sc.ScoreCard:keypressed(key)
  -- Control player name text input here.
  -- If they hit enter, save and boot them back to the level select screen.

  if key == 'left' then
    self.state.scoreNameInput:moveLeft()
  end

  if key == 'right' then
    self.state.scoreNameInput:moveRight()
  end

  if key == 'backspace' then
    self.state.scoreNameInput:backspace()
  end

  -- When they hit enter, store their name and score, and change scene.
  -- If they didn't enter a name, they get to be "?".
  if key == 'return' then
    local name = self.state.scoreNameInput.text
    if utf8.len(name) == 0 then
      name = '?'
    end

    persistence.addScore(self.state.levelId, name, self.state.scores[self.state.scoreIndex].score)

    scenes.change(require('sc.level_select').new()) -- XXX: Hacking around circular dependencies.
  end
end

return sc.ScoreCard
