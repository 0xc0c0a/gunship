local class = require 'class'

local Vector = class{}

function Vector.construct(instance, x, y)
  instance.x = x or 0
  instance.y = y or 0
end

function Vector.addVector(a, b)
  return Vector.new(a.x+b.x, a.y+b.y)
end

function Vector.subVector(a, b)
  return Vector.new(a.x-b.x, a.y-b.y)
end

function Vector.mulScalar(v, s)
  return Vector.new(v.x*s, v.y*s)
end

function Vector.magnitude(v)
  return math.sqrt(v.x*v.x + v.y*v.y)
end

function Vector.normalize(v)
  local magnitude = Vector.magnitude(v)
  return Vector.new(v.x / magnitude, v.y / magnitude)
end

function Vector.toString(v)
  return string.format('(%.2f, %.2f)', v.x, v.y)
end

return Vector
