local e = {}

e.string = {}

function e.string.starts(s, start)
  return s:sub(1, start:len()) == start
end

return e.string
