local clone = require 'clone'

return function(prototype)
  local class = clone(prototype)

  class.new = function(...)
    local instance = clone(class)
    class.construct(instance, ...)

    return instance
  end

  class.construct = prototype.construct or function(instance, ...) end

  return class
end
